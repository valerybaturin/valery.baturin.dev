module.exports = {
  purge: ["./pages/**/*.{js,ts,jsx,tsx}", "./components/**/*.{js,ts,jsx,tsx}"],
  darkMode: "media", // or 'media' or 'class' of false
  theme: {
    fontFamily: {
      body: "Quicksand, sans-serif",
      sans: "'Patua One', cursive",
    },
    extend: {
      gridTemplateColumns: {
        'skills': "minmax(min-content, max-content) 1fr",
        'edu': "minmax(min-content, max-content) 1fr minmax(min-content, max-content)",
        'links': "minmax(min-content, max-content) minmax(min-content, max-content)",
        'work': "minmax(min-content, max-content) minmax(min-content, max-content) minmax(min-content, max-content)",
      },
      screens: {
        print: {raw: "print"},
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
};
