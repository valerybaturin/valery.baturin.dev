import Head from "next/head";
import styles from "../styles/Home.module.css";
import React from "react";
import moment from "moment";

export default function Home() {
  const now = Date.now()
  const date = moment(now).diff(moment([2022, 7, 1]), 'months', true)

  return (
    <div className="container mx-auto max-w-screen-lg">
      <Head>
        <title>Valery Baturin | Go Developer</title>
        <meta name="description" content="Valery Baturin | Go Developer" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className="p-6 md:p-4">
        {/* Name */}
        <span className={styles.title}>Valery Baturin</span>
        <div className="grid grid-rows-4 grid-cols-1 md:grid-rows-2 md:grid-cols-links gap-x-8">
          <div>
            <a href="https://valery.baturin.codes">valery.baturin.dev</a>
          </div>
          <div>
            <a href="mailto:valery@baturin.codes">valery@baturin.codes</a>
          </div>
          <div>
            <a href="https://linkedin.com/in/valerybaturin">
              linkedin.com/in/valerybaturin
            </a>
          </div>
          <div>
            <a href="https://github.com/valerybaturin">
              github.com/valerybaturin
            </a>
          </div>
        </div>

        {/* Skills Section */}
        <section className={styles.section}>
          <h2 className={styles.subtitle}>Skills</h2>
          <div className={styles.skills}>
            <div>Languages:</div>
            <div>Go, SQL, HTML / CSS / JavaScript</div>
            <div>Data:</div>
            <div>PostgreSQL, Yugabyte, MongoDB, Redis, RabbitMQ</div>
            <div>Transport:</div>
            <div>REST, gRPC, Protocol Buffers</div>
            <div>DevOps:</div>
            <div>Linux, Docker, Kubernetes, GitLab CI/CD, Prometheus, Grafana, Sentry</div>
            <div>Architecture:</div>
            <div>Microservices, Sync / Async API, DDD, Monolith Decomposition Patterns</div>
          </div>
        </section>
        {/*  Work Experience */}
        <section className={styles.section}>
          <h2 className={styles.subtitle}>Work Experience</h2>

          <div className={styles.work}>
            <div className="text-left">Senior Go Backend Developer</div>
            <div className="text-left md:text-center">
              <a href="https://sbercloud.ru">Cloud</a>
            </div>
            <div className="flex justify-between md:justify-end">
              <div className="order-1 md:order-2">2022 — Current</div>
              <div className="order-2 md:order-1 md:pr-4 md:text-gray-500 md:font-light">
                {date < 2 ? (
                  Math.round(date) + ' month'
                ) : (
                  Math.round(date) + ' months'
                )}
              </div>
            </div>
          </div>
          <div className="pb-10">
            <ul className={styles.bullets}>
              <li>
                Rebuilt company's URL shortener from scratch, reducing memory usage more than 3x and eliminating errors.
              </li>
              <li>
                Removed a single point of failure by applying monolith decomposition pattern which helped to efficiently move current architecture to microservices.
              </li>
              <li>
                Introduced Go best practices and project layout template to Go developers in the company. This significantly increased maintainability in other teams.
              </li>
              <li>
                Added Four Golden Signals to microservices' metrics which helped team respond on failures faster. This decreased the number of incidents by a factor of 2.
              </li>
            </ul>
          </div>

          <div className={styles.work}>
            <div className="text-left">Go Backend Developer</div>
            <div className="text-left md:text-center">
              <a href="https://sbercloud.ru">Cloud</a>
            </div>
            <div className="flex justify-between md:justify-end">
              <div className="order-1 md:order-2">2021 — 2022</div>
              <div className="order-2 md:order-1 md:pr-4 md:text-gray-500 md:font-light">
                10 months
              </div>
            </div>
          </div>
          <div className="pb-10">
            <ul className={styles.bullets}>
              <li>
                Introduced Clean Architecture and SOLID principles to the team. This allowed developers to write more understandable and reliable code. Reduced the time spent on debugging by 2x.
              </li>
              <li>
                Refactored website's backend which reduced answer latency by more than 4x via implementing goroutines with channels.
              </li>
              <li>
                Refactored database layer by implementing reusability. This code base reduced time spent on development from weeks to days.
              </li>
              <li>
                Increased database performance by more than 2x via optimizing SQL queries and building appropriate indexes. In some cases latency wes decreased from seconds to milliseconds.
              </li>
              <li>
                Added security layer to the microservice to allow different users have different access rights by implementing JWT and roles.
              </li>
              <li>
                Interviewed backend developers for the team, made decisions about hiring a candidate.
              </li>
            </ul>
          </div>

          <div className={styles.work}>
            <div className="text-left">Full-Stack Developer (MarCom)</div>
            <div className="text-left md:text-center">
              <a href="https://trimble.com">Trimble Inc.</a>
            </div>
            <div className="flex justify-between md:justify-end">
              <div className="order-1 md:order-2">2015 — 2021</div>
              <div className="order-2 md:order-1 md:pr-4 md:text-gray-500 md:font-light">
                6 years
              </div>
            </div>
          </div>

          <div>
            <ul className={styles.bullets}>
              <li>
                Led the team of 6 people from different company divisions to migrate all
                local web resources to a distributed services architecture to unify development workflow,
                reduce codebase size, simplify dependency management and remove
                legacy code. Started to adopt Go language, Kubernetes and CI/CD
                workflow.
              </li>
              <li>
                Reduced order processing time from hours to minutes and
                eliminated manual email processing by creating a REST API for
                orders. Increased managers' efficiency by 80% through creating a
                web application with intuitive interface on PHP / Laravel /
                MySQL / Bootstrap.
              </li>
              <li>
                Reduced update time of resellers' online information from days to hours by building a new web
                application and utilizing an existing REST API. Tech: React / Next.js / TailwindCSS
              </li>
              <li>
                Built regional community by creating brand's websites for
                resellers and end-users. Increased traffic to websites by more
                than 60% year-over-year. Tech: Ghost CMS / Discourse
              </li>
              <li>
                Marketing activity for 9 countries. Designed marketing
                materials and giveaways. Managed newsletters and social media pages in
                Facebook, Instagram and VK.
              </li>
            </ul>
          </div>
        </section>

        {/*  Education */}
        <section>
          <h2 className={styles.subtitle}>Education</h2>
          <div className={styles.education}>
            <div className="flex flex-wrap">
              <div className="md:w-3/4 font-bold md:order-1">
                Master's Degree, Information Systems and Technologies
              </div>
              <div className="w-full md:order-3">
                Moscow State Institute of Radio Engineering, Electronics and
                Automation
              </div>
              <div className="text-gray-500 dark:text-gray-200 font-bold md:order-2 md:text-right md:w-1/4">
                2010 — 2013
              </div>
            </div>
            <div className="flex flex-wrap">
              <div className="md:w-3/4 font-bold md:order-1">
                Engineer's Degree, Information Systems and Technologies
              </div>
              <div className="w-full md:order-3">
                Moscow State Institute of Radio Engineering, Electronics and
                Automation
              </div>
              <div className="text-gray-500 dark:text-gray-200 font-bold md:order-2 md:text-right md:w-1/4">
                2004 — 2010
              </div>
            </div>
          </div>
        </section>

        {/*  Projects */}
        <section className={styles.section}>
          <div className={styles.subtitle}>Volunteer Projects</div>
          <div>
            <div className="flex flex-wrap w-full justify-between">
              <div className="lg:w-5/12 font-bold">Full-Stack Developer</div>
              <div className="lg:w-5/12 text-left">
                <a href="https://gkmosolov.foundation">
                  G.K. Mosolov Foundation
                </a>
              </div>
              <div className="lg:w-2/12 text-gray-500 dark:text-gray-200 font-bold text-right">
                2020 — 2022
              </div>
              <div>
                <ul className={styles.bullets}>
                  <li>
                    Helped to start sales by creating an online store for
                    Charity Foundation.
                  </li>
                  <li>
                    Rebuilt backend to microservices architecture to simplify
                    maintainability and deploy workflow. Tech: Go / Nuxt.js /
                    REST / RabbitMQ / PostgreSQL
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </section>
      </main>
    </div>
  );
}
